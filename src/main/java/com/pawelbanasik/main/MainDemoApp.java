package com.pawelbanasik.main;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.pawelbanasik.config.DemoConfig;
import com.pawelbanasik.dao.AccountDao;
import com.pawelbanasik.dao.MembershipDao;
import com.pawelbanasik.domain.Account;

public class MainDemoApp {

	public static void main(String[] args) {

		// read spring config java class
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(DemoConfig.class);

		// get the bean from spring container
		AccountDao accountDao = context.getBean("accountDao", AccountDao.class);

		// get the bean
		MembershipDao membershipDao = context.getBean("membershipDao", MembershipDao.class);

		Account account = new Account();
		
		// call the business method
		accountDao.addAccount(account, true);
		accountDao.doWork();
		// call the membership business method
		membershipDao.addSillyMember();
		membershipDao.goToSleep();
		// close the context
		context.close();

	}

}
