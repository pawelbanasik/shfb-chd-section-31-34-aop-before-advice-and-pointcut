package com.pawelbanasik.aspect;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class MyDemoLoggingAspect {

	// fully qualified name
	// @Before("execution(public void
	// com.pawelbanasik.dao.AccountDao.addAccount())")
	// public void beforeAddAccountAdvice() {
	// System.out.println("\n=====> Executing @Before advice on addAccount()");
	//
	// }

	// with star wildcard
	// @Before("execution(public void add*())")
	// public void beforeAddAccountAdvice() {
	// System.out.println("\n=====> Executing @Before advice on addAccount()");
	//
	// }

	// just return type and wildcard
	// @Before("execution(void add*())")
	// public void beforeAddAccountAdvice() {
	// System.out.println("\n=====> Executing @Before advice on addAccount()");
	//
	// }

	// almost everything as wildcard
	// @Before("execution(* add*())")
	// public void beforeAddAccountAdvice() {
	// System.out.println("\n=====> Executing @Before advice on addAccount()");
	//
	// }

	// @Before("execution(* add*(..))")
	// public void beforeAddAccountAdvice() {
	// System.out.println("\n=====> Executing @Before advice on addAccount()");
	//
	// }

	@Before("execution(* com.pawelbanasik.dao.*.*(..))")
	public void beforeAddAccountAdvice() {
		System.out.println("\n=====> Executing @Before advice on addAccount()");

	}

}
